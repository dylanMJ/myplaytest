import models.User;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * 启动任务
 */

@OnApplicationStart
public class Bootstrap extends Job {

    public void doJob()
    {
        // 检查数据库是否为空
        if(User.count() == 0)
        {
            System.out.println("init  data base - initial-data.yml");
            Fixtures.loadModels("initial-data.yml");
        }
    }
}

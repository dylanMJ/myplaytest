package models;


import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;

/**
 * @describe 用户对象
 */

@Entity
public class User extends Model {

    @Required
    public String email;

    @Required
    public String password;
    public String fullname;
    public boolean isAdmin;

    public User(String eamil, String password, String fullname, boolean isAdmin) {
        this.email = eamil;
        this.password = password;
        this.fullname = fullname;
        this.isAdmin = isAdmin;
    }

    public static User connect(String email, String password) {
        return find("byEmailAndPassword", email, password).first();
    }


    public String toString() {
        return email;
    }
}
package controllers;


import models.User;

public class Security extends Secure.Security{


    /*
     * 后台 登陆验证
     */
    public static boolean authenticate(String username,String password)
    {

        return User.connect(username,password) != null ;
    }


    /*
     * 登陆成功
     */
    public static void onAuthenticated()
    {

               Admin.index();
               //Application.index();
    }

    /*
     * 注销
     */
    public static void OnDisconnected()
    {

        System.out.println("username:   password : ");
        Application.index();
    }



    /*
     * 通过 邮箱查询 是否 超级管理员
     */
    public static boolean check(String profile) {

            if( "admin".equals(profile) )
            {
                return User.find("byEmail", connected()).<User>first().isAdmin;
            }

            return false;
    }
}

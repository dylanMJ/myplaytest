package controllers;

import play.*;
import play.cache.Cache;
import play.data.validation.Required;
import play.libs.Codec;
import play.libs.Images;
import play.mvc.*;

import java.util.*;

import models.*;

public class Application extends Controller {


    /*
     * 博客首页数据
     */
    public static void index()
    {
        Post frontPost = Post.find("order by postedAt desc").first();

        List<Post> olderPosts = Post.find("order by postedAt desc").from(1).fetch(10);


        render(frontPost, olderPosts);
    }


    /*
     * 查看评论
     */
    public static void show(Long  id)
    {

        Post post = Post.findById(id) ;

        //生成一个唯一的ID
        String randomID = Codec.UUID();

        render( post,randomID );
    }



     /*
     *验证码
     */
    public static void captcha(String id) {
        Images.Captcha captcha = Images.captcha();
        String code = captcha.getText("#E4EAFD");
        Cache.set(id, code, "10mn");
        renderBinary(captcha);
    }

    /*
         * 添加 评论
         */
     public static void postComment(
                Long postId,
                @Required(message = "作者不能为空") String author,
                @Required(message = "留言内容不能为空") String content,
                @Required(message = "请输入验证码") String code,
                String randomID
        ) {

            Post post = Post.findById(postId);

            validation.equals( code, Cache.get(randomID) ).message("验证码错误，请从新输入");


            if( validation.hasErrors() )
            {
                render("Application/show.html", post,randomID,author,content);
            }

            post.addComment(author, content);
            flash.success("Thanks for posting %s", author);
            Cache.delete(randomID);

            show(postId);
    }

    public static void listTagged(String tag) {
           List<Post> posts = Post.findTaggedWith(tag);
           render(tag, posts);
       }
}